import React from 'react';
const StatelessComponent=(props)=> {

    return (<div> <h2> Stateless Component {props.age}</h2>
    <button onClick={props.update}>Click </button> </div>)
}
export default  StatelessComponent;