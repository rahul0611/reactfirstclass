import React from "react";
import StatelessComponent from "./StatelessComponent";
class StatefulComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Rahul",
      counter : 0
    };
  }
 

  update=()=> {
      this.state.counter= this.state.counter+1;
     // alert(this.state.counter);
      this.setState({name:this.state.counter})
  }

  render() {
    return (
      <div>
        <h1> Welcome to the Learning Session !! {this.state.counter}</h1>
        <StatelessComponent update={this.update}/>
      </div>
    );
  }
}

export default StatefulComponent;
